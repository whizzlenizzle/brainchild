"""
Characters

Characters are (by default) Objects setup to be puppeted by Players.
They are what you "see" in game. The Character class in this module
is setup to be the "default" character type created by the default
creation commands.

"""
from evennia import DefaultCharacter
from world import races
from world import rulebook
from world.traits.trait import Trait


class Character(DefaultCharacter):
    """
    The Character defaults to implementing some of its hook methods with the
    following standard functionality:

    at_basetype_setup - always assigns the DefaultCmdSet to this object type
                    (important!)sets locks so character cannot be picked up
                    and its commands only be called by itself, not anyone else.
                    (to change things, use at_object_creation() instead)
    at_after_move - launches the "look" command
    at_post_puppet(player) -  when Player disconnects from the Character, we
                    store the current location, so the "unconnected" character
                    object does not need to stay on grid but can be given a
                    None-location while offline.
    at_pre_puppet - just before Player re-connects, retrieves the character's
                    old location and puts it back on the grid with a "charname
                    has connected" message echoed to the room

    """
    def at_object_creation(self):
        self.db.race = None
        self.db.slots = {}
        self.db.skills = {}
        self.db.stats = {
            'strength': Trait('strength', static=True),
            'dexterity': Trait('dexterity', static=True),
            'constitution': Trait('constitution', static=True),
            'intelligence': Trait('intelligence', static=True),
            'wisdom': Trait('wisdom', static=True),
            'charisma': Trait('charisma', static=True)
        }

    def become_race(self, race):
        self.db.race = races.load_race(race)
        for slot in self.db.race.slots:
            self.db.slots[slot] = None

    def setup_stats(self, priorities):
        if len(priorities) == len(self.db.stats):
            stat_rolls = rulebook.roll_stats(len(self.db.stats))
            for i, stat in enumerate(priorities):
                if stat in self.db.stats:
                    self.db.stats[stat].base = stat_rolls[i]
                else:
                    return False
            return True
        else:
            return False

    @property
    def size(self):
        if self.db.race:
            return self.db.race.get_size()
