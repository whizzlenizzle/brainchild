from world.races.race import Race, RaceSize, RaceType


class Dragon(Race):
    def __init__(self):
        super(Dragon, self).__init__()
        self.name = "Dragon"

        self.bonuses = {
            'strength': 2,
            'intelligence': 3
        }

        self.size = RaceSize.colossal
        self.type = RaceType.quadruped

        self.limbs = [
            'head',
            'neck',
            'body',
            'left wing',
            'right wing',
            'front left leg',
            'front right leg',
            'back left leg',
            'back right leg',
            'front left claw',
            'front right claw',
            'back left claw',
            'back right claw'
        ]

        self.limb_pluralization = {
            'wing': 'wings',
            'leg': 'legs',
            'claw': 'claws'
        }

        self.generate_slot_map()
