from world.races.race import Race, RaceSize


class Giant(Race):
    def __init__(self):
        super(Giant, self).__init__()
        self.name = "Giant"

        self.size = RaceSize.gargantuan

        self.generate_slot_map()
