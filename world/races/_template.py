from world.races.race import Race, RaceSize, RaceType


"""
How to make a new race:
1) Copy this file
2) Rename the file to the name of your race (lowercase)
3) Edit the below code to your liking (all defaults are humanoid)
4) You can then load the race with load_race

Reference:

RACE SIZES:
RaceSize.fine           (1/8)
RaceSize.diminuitive    (1/6)
RaceSize.tiny           (1/4)
RaceSize.small          (1/2)
RaceSize.medium         (human, 1)
RaceSize.large          (2)
RaceSize.huge           (4)
RaceSize.gargantuan     (8)
RaceSize.colossal       (16)

RACE TYPES:
RaceType.biped
RaceType.quadruped
RaceType.beast (same as quadruped, technically)
"""


class NewRace(Race):
    def __init__(self):
        super(NewRace, self).__init__()
        self.name = "NewRace"

        # Uncomment to add bonuses and detriments
        # self.bonuses = {}
        # self.detriments = {}

        # Below are the defaults for type and size, uncomment only if you want
        # to change them.
        # self.size = RaceSize.medium
        # self.type = RaceType.biped

        # Only directly define self.libms if you are completely overhauling
        # humanoid limbs.
        # Otherwise, use add_limb or add_limbs or remove_limb or remove_limbs
        # self.limbs = []

        # Inform the race which types of limbs to group:
        # self.limb_pluralization = {}

        # This must be called last
        self.generate_slot_map()

        # Then you may add additional slots, if you wish
        # self.add/remove_slot(s)
