from enum import Enum, unique
from config import DEFAULT_LIMBS


class Race(object):
    """
    Documentation
    """
    def __init__(self):
        self.name = ""
        self.size = RaceSize.medium
        self.type = RaceType.biped
        self.limbs = DEFAULT_LIMBS
        self.slots = {}
        self.bonuses = {}
        self.detriments = {}
        self.limb_pluralization = {
            'arm': 'arms',
            'leg': 'legs',
            'hand': 'hands',
            'foot': 'feet'
        }

    def get_size(self):
        return self.size.value[self.type.value]

    def set_size(self, racesize):
        if isinstance(racesize, RaceSize):
            self.size = racesize
        else:
            return False

    def add_limb(self, limb):
        self.limbs.append(limb)

    def add_limbs(self, limbs):
        self.limbs = self.limbs + limbs

    def remove_limb(self, limb):
        self.limbs.pop(self.limbs.index(limb))

    def remove_limbs(self, limbs):
        for limb in limbs:
            self.remove_limb(limb)

    def generate_slot_map(self):
        for limb in self.limbs:
            if limb in self.slots:
                continue
            elif any(word in limb for word in self.limb_pluralization):
                key = limb.rsplit(None, 1)[-1]
                lp = self.limb_pluralization[key]
                if lp not in self.slots:
                    self.slots[lp] = []
                    self.slots[lp].append(limb)
                else:
                    self.slots[lp].append(limb)
            else:
                self.slots[limb] = [limb]


@unique
class RaceSize(Enum):
    fine = [.125, .25]
    diminuitive = [.25, .5]
    tiny = [.5, .75]
    small = [.75, 1]
    medium = [1, 1.5]
    large = [2, 3]
    huge = [4, 6]
    gargantuan = [8, 12]
    colossal = [16, 24]


class RaceType(Enum):
    biped = 0
    beast = 1
    quadruped = 1
