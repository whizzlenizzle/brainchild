from random import randint


CARRYING_CAPACITY = [
    [0, 0, 0],
    [3, 6, 10],
    [6, 13, 20],
    [10, 20, 30],
    [13, 26, 40],
    [16, 33, 50],
    [20, 40, 60],
    [23, 46, 70],
    [26, 53, 80],
    [30, 60, 90],
    [33, 66, 100],
    [38, 76, 115],
    [43, 86, 130],
    [50, 100, 150],
    [58, 116, 175],
    [66, 133, 200],
    [76, 153, 230],
    [86, 173, 260],
    [100, 200, 300],
    [116, 233, 350],
    [133, 266, 400],
    [153, 306, 460],
    [173, 346, 520],
    [200, 400, 600],
    [233, 466, 700],
    [266, 533, 800],
    [306, 613, 920],
    [346, 693, 1040],
    [400, 800, 1200],
    [466, 933, 1400]
]


def get_carry_capacity(size, strength):
    while strength > 29:
        strength -= 10
        size *= 4

    return [
        CARRYING_CAPACITY[strength][0] * size,
        CARRYING_CAPACITY[strength][1] * size,
        CARRYING_CAPACITY[strength][2] * size
    ]


def roll_stat():
    rolls = []
    stat = 0

    # roll a 4d6
    for stat in range(0, 4):
        rolls.append(randint(1, 6))
    # order from high to low
    rolls = sorted(rolls, reverse=True)
    # drop lowest roll
    rolls.pop()

    for roll in rolls:
        stat += roll

    return stat


def roll_stats(amount):
    stats = []

    for stat in range(0, amount):
        stats.append(roll_stat())

    return sorted(stats, reverse=True)


def roll_percent(success):
    d1 = randint(0, 9)
    d2 = randint(0, 9)
    p = None

    d1 *= 10

    if d2 == 0:
        p = d1 + 10
    else:
        p = d1 + d2

    if p <= success:
        return True
    else:
        return False
