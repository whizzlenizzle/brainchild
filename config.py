# Game settings

GAMENAME = "Brainchild"

# These will be the default limbs unless you specify otherwise on a race
DEFAULT_LIMBS = [
    'head',
    'torso',
    'left arm',
    'right arm',
    'left hand',
    'right hand',
    'left leg',
    'right leg',
    'left foot',
    'right foot'
]
